import setuptools

with open("readme.md", "r") as fh:
    long_description = fh.read()

from src.version import __version__ as version

setuptools.setup(
    name="netzr", # Replace with your own username
    version=version,
    author="blurryroots",
    author_email="blurryroots@think-biq.com",
    description="CLI tool for managing wifi connections.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/think-biq/netzr",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': ['netzr = src.netzr.cli:cli'],
    }
)