#python3 -m pip install --user --upgrade setuptools wheel

FILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_DIR := $(shell dirname $(FILE_PATH))
PROJECT_NAME := $(notdir $(patsubst %/,%,$(dir $(FILE_PATH))))
PROJECT_VERSION := $(shell python3 -c "from src.version import get_version; print(get_version())")
ifndef no-venv
	VENV_CMD = . "$(PROJECT_DIR)/bin/activate"
else
	VENV_CMD = true
endif

default: clean venv deps build

debug:
	@echo "FILE_PATH: $(FILE_PATH)"
	@echo "PROJECT_DIR: $(PROJECT_DIR)"
	@echo "PROJECT_NAME: $(PROJECT_NAME)"
	@echo "PROJECT_VERSION: $(PROJECT_VERSION)"
	@echo "VENV_CMD: $(VENV_CMD)"

venv:
	@echo "Creating venv ..."
	@python3 -m venv --upgrade "$(PROJECT_DIR)"
	@${VENV_CMD}; python3 -m pip install -U pip

deps:
	@echo "Resolving dependencies ..."
	@${VENV_CMD}; python3 -m pip install -r "$(PROJECT_DIR)/requirements.txt"

build:
	@echo "Building wheel ..."
	@${VENV_CMD}; python3 -m pip install -U setuptools wheel; python3 "$(PROJECT_DIR)/setup.py" sdist bdist_wheel

clean:
	@rm -rf "$(PROJECT_DIR)/build"
	@rm -rf "$(PROJECT_DIR)/dist"

rebuild: clean build

install:
	@echo "Installing local wheel into venv for $(PROJECT_NAME) v$(PROJECT_VERSION) ..."
	@${VENV_CMD}; python3 -m pip install -U "$(PROJECT_DIR)/dist/$(PROJECT_NAME)-$(PROJECT_VERSION)-py3-none-any.whl"

set-version:
	echo "__version__ = '$(version)'" > "$(PROJECT_DIR)/src/__init__.py"

download-build:
	curl -L --output "netzer-latest.zip" --user-agent "X-netzr-dev" "https://gitlab.com/think-biq/netzr/-/jobs/artifacts/master/download?job=build"