import subprocess

def run(params):
    if None is params or 0 == len(params):
        raise Exception('Could not run command, empty paramter list!')
        
    p = subprocess.Popen(params,
            bufsize=0,
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE)

    out, err = p.communicate()

    return (p.returncode, out, err)