from src.lola.run import run as lola_run
from .base import NetzerBase
import logging

class Netzer(NetzerBase):
    LOG = logging.getLogger('netzer')
    AIRPORT = '/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport'

    def status(self, interface = 'en0'):
        #networksetup -getairportnetwork en0
        params = ['networksetup', '-getairportnetwork', interface]
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('networksetup returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        name_info = out.decode('utf8')

        params = ['networksetup', '-getairportpower', interface]
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('networksetup returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        state_info = out.decode('utf8')

        params = ['sudo', self.AIRPORT, interface, '-I']
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('airport returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        bssid_key = 'BSSID: '
        bssid = 'UNKNOWN'
        for line in out.decode('utf8').split('\n'):            
            p = line.find(bssid_key)
            if -1 < p:
                 bssid = line[p + len(bssid_key):].strip()

        return name_info + state_info + f'BSSID: {bssid}'

    def disconnect(self, interface = 'en0'):
        params = ['sudo', self.AIRPORT, interface, '-z']
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('airport returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        return out.decode('utf8')


    def connect(self, ssid, interface = 'en0'):
        #'networksetup -setairportnetwork en0 <SSID_OF_NETWORK>'
        params = ['networksetup', '-setairportnetwork', interface, ssid]
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('networksetup returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        return out.decode('utf8')

    def recover_password(self, ssid):
        params = ['security', 'find-generic-password', '-wa', ssid]
        code, out, err = lola_run(params)
        if 0 != code:
            Netzer.LOG.warning('security returned with error')
            Netzer.LOG.error(err.decode('utf8'))

        return out.decode('utf8')

    def discover(self):
        code, out, err = lola_run([self.AIRPORT, '-s'])
        if 0 != code:
            Netzer.LOG.warning('airport returned with error')
            Netzer.LOG.error(err)
            return []

        results = []
        i = 0
        lines = out.decode('utf8').split('\n')
        header = lines[0]
        name_start = 0
        name_end_pos = header.find('SSID') + len('SSID')
        bssid_start = header.find('BSSID')
        rssi_start = header.find('RSSI')
        channel_start = header.find('CHANNEL')
        ht_start = header.find('HT')
        cc_start = header.find('CC')
        security_start = header.find('SECURITY')


        lines = lines[1:-1] # remove header and empty end line
        for i in range(0, len(lines)):
            line = lines[i]
            ssid = line[name_start:name_end_pos].strip()
            bssid = line[bssid_start:rssi_start-1].strip()
            rssi = int(line[rssi_start:channel_start-1].strip())
            channel = (line[channel_start:ht_start-1].strip())
            ht = line[ht_start:cc_start-1].strip()
            cc = line[cc_start:security_start-1].strip()
            security = line[security_start:].strip()
            entry = {
                'ssid': ssid,
                'bssid': bssid,
                'rssi': rssi,
                'channel': channel,
                'ht': ht,
                'cc': cc,
                'security': security
            }
            results.append(entry)

        return results
