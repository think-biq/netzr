import json
import argparse

# TODO: make this dependent on os
from .osx import Netzer

from ..version import get_version

#
n = Netzer()

def discover(dump_json, only_ssid):
    networks = n.discover()
    if dump_json:
        print(json.dumps(networks))
    else:
        if only_ssid:
            for net in networks:
                print(net['ssid'])
        else:
            for net in networks:
                out = ''
                for key in net.keys():
                    out += f'{key}: {net[key]}; '
                print(out)

def recover_password(ssid):
    pw = n.recover_password(ssid)
    print(pw)

def connect_wifi(ssid):
    r = n.connect(ssid)
    print(r)

def disconnect_wifi():
    r = n.disconnect()
    print(r)

def status():
    r = n.status()
    print(r)

def version():
    v = get_version()
    print(v)

def main(args):
    if args.version:
        version()
        return 0

    if 'recover' in args.command:
        recover_password(args.ssid)
    elif 'discover' in args.command:
        discover(args.export_json, args.only_ssid)
    elif 'disconnect' in args.command:
        disconnect_wifi()
    elif 'connect' in args.command:
        connect_wifi(args.ssid)
    elif 'status' in args.command:
        status()

    return 0

def cli():
    main_parser = argparse.ArgumentParser()
    main_parser.add_argument('-v', '--version', action="store_true", default = False)

    subparsers = main_parser.add_subparsers(dest='command')

    status_parser = subparsers.add_parser('status')

    recover_parser = subparsers.add_parser('recover')
    recover_parser.add_argument('ssid', type=str)

    discover_parser = subparsers.add_parser('discover')
    discover_parser.add_argument('-j', '--export-json', action="store_true", default = False)
    discover_parser.add_argument('-s', '--only-ssid', action="store_true", help='only show ssid', default = False)

    connect_parser = subparsers.add_parser('connect')
    connect_parser.add_argument('ssid', type=str)

    disconnect_parser = subparsers.add_parser('disconnect')

    main(main_parser.parse_args())

if __name__ == '__main__':
    cli()