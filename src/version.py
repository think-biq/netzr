from .__init__ import __version__ as _version

__version__ = _version

def get_version():
	return __version__

if __name__ == '__main__':
	print(get_version())